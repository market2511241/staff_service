package grpc

import (
	"market/staff_service/config"
	"market/staff_service/gRPC/client"
	"market/staff_service/gRPC/service"
	"market/staff_service/genproto/staff_service"
	"market/staff_service/pkg/logger"
	"market/staff_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	staff_service.RegisterStaffTariffServiceServer(grpcServer, service.NewStaffTariffService(cfg, log, strg, srvc))
	staff_service.RegisterStaffServiceServer(grpcServer, service.NewStaffService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
